import winston from 'winston';
import 'winston-daily-rotate-file';
import path from 'path';
import * as Constants from '../Constants.js';

export default winston.createLogger({
    format: winston.format.combine(
        winston.format.splat(),
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.colorize(),
        winston.format.printf(
            log => {
                if (log.stack) return `[${log.timestamp}] [${log.level}] ${log.stack} \n`;
                return `[${log.timestamp}] [${log.level}] ${JSON.stringify(log.message)} \n`;
            },
        ),
    ),
    transports: [
        new winston.transports.Console({ json: true }),
        new winston.transports.DailyRotateFile({
            filename: '%DATE%',
            datePattern: "YYYYMMDD[.log]",
            dirname: path.join(Constants.__dirname, "logs/"),
            colorize: true,
            json: true,
            maxSize: '20m',
            maxFiles: '2d'
        })
    ],
})