import mysql from 'mysql';
import * as Constants from '../Constants.js';
import logger from './logger.js';

function stablishedConnection() {
    try {
        return new Promise((resolve, reject) => {
            const con = mysql.createConnection({
                host: Constants.MySql.Host,
                user: Constants.MySql.User,
                password: Constants.MySql.Password,
                database: Constants.MySql.Database
            });

            con.connect((err) => {
                if (err) reject(err);
                resolve(con);
            });

        });

    } catch (error) {
        throw error;
    };
}

export function executeQuery(query = '', data) {
    try {
        return new Promise(async function (resolve, reject) {
            var conn = await stablishedConnection()
                .catch((err) => {
                    reject(err);
                });

            if (data != undefined) {
                conn.query(query, data, async function (err, data) {
                    if (err) reject(err);
                    resolve(data);
                });
            } else {
                conn.query(query, async function (err, data) {
                    if (err) reject(err);
                    resolve(data);
                });
            }
            conn.end();
        });

    } catch (error) {
        throw error;
    };
}

export function closeDbConnection(con) {
    if (con != undefined) {
        if (con.state == 'authenticated') {
            con.end();
            con.destroy();
        }
    }
}