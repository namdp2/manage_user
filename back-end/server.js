import express from 'express';
import bodyParser from 'body-parser';
import * as Constants from './Constants.js';
import routes from './api/routes.js';
import logger from './helper/logger.js';
import cors from 'cors';

let app = express();
let port = Constants.Port;

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors({
    origin: 'http://localhost:9111'
}));
routes(app);

app.listen(port);
logger.info('RESTful API server started on: ' + port);
