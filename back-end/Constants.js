import dotenv from "dotenv";
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
export const __dirname = path.dirname(__filename);
dotenv.config({
    path: path.resolve(__dirname, '.env.' + process.env.Environment)
});

export const Port = process.env.Port;
// Mysql
export const MySql = {
    Database: process.env.MySqlDB,
    Host: process.env.MySqlHost,
    Port: process.env.MySqlPort,
    User: process.env.MySqlUser,
    Password: process.env.MySqlPassword
}