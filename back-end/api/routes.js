import route from './routes/Example.js';

export default function routes(app) {

    app.use('/v1', route);

};