import { executeQuery } from '../../helper/mysql.js';
import * as Constants from '../../Constants.js';


export async function Get(req, res) {
    try {
        var data = await executeQuery(`SELECT Id, Code, Name, Birthday, Image FROM Sample`);
        return res.json({
            status: true,
            data: data
        })
    } catch (error) {
        return res.json({
            status: false,
            message: error.message
        });
    }
}

export async function Add(req, res) {
    try {
        var data = await executeQuery(
            `INSERT INTO sample.sample (Code, Name, CreatedDate, Birthday, Image) 
            VALUES(?, ?, ?, ?, ?)`,
            [req.body.Code, req.body.Name, new Date(), req.body.Birthday, req.body.Image]);
        return res.json({
            status: true
        })
    } catch (error) {
        return res.json({
            status: false,
            message: error.message
        });
    }
}

export async function Update(req, res) {
    try {
        var data = await executeQuery(
            `UPDATE sample.sample
            SET Code=?, Name=?, Birthday=?, Image=?
            WHERE Id=?`,
            [req.body.Code, req.body.Name, req.body.Birthday, req.body.Image,
            req.body.Id]);
        return res.json({
            status: true
        })
    } catch (error) {
        return res.json({
            status: false,
            message: error.message
        });
    }
}

export async function Delete(req, res) {
    try {
        var data = await executeQuery(`DELETE FROM Sample WHERE Id=?`, [req.body.Id]);
        return res.json({
            status: true
        })
    } catch (error) {
        return res.json({
            status: false,
            message: error.message
        });
    }
}