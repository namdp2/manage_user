import express from 'express';
import * as example from '../controller/ExampleController.js';


var router = express.Router();

router.post('/add', example.Add);
router.post('/update', example.Update);
router.post('/delete', example.Delete);
router.get('/get', example.Get);

export default router;
