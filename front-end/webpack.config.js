const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin")
const CopyWebpackPlugin = require('copy-webpack-plugin')

const DIST = path.resolve(__dirname, 'dist')

module.exports = {
  mode: 'development',
  entry: {
    index: './src/index.js'
  },
  output: {
    filename: (data) => {
      return `index.${data.hash}.js`
    },
    path: DIST
  },
  devServer: {
    port: 9111,
    allowedHosts: "all",
    compress: true,
    devMiddleware: {
      writeToDisk: true
    },
    static: {
      directory: DIST
    },
    hot: false
  },
  plugins: [
    new NodePolyfillPlugin(),
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
      cleanOnceBeforeBuildPatterns: ['**/*', '!assets/**']
    }),    
        // for build scripts
        new CopyWebpackPlugin({
          patterns: [
              {
                  from: "./src/",
                  globOptions: {
                      ignore: ['**/*.js', '**/*.html'],
                  }
              },
              {
                  from: "./src/assets/js",
                  to: `${DIST}/assets/js`
              }
          ],
      }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: "body",
      chunks: ['index'],
      filename: 'index.html'
    }),
  ],
  resolve: {
    fallback: {
      "fs": false
    },
  }
}
