// study 1 (5-7 ngày)
// Đủ tính năng thêm, sửa, xóa, view
// 1. view: array, lấy data từ api --> như demo bên dưới
// - View table, list, div, ....... <-- thao tác array, json
// 2. add: 2 input Code và Name
// - Post tới api, truyền json
// 3. update: 2 input + 1 hidden (lưu id)
// 4. delete: hidden input (luu id)

// api
// 1. http://localhost:4000/v1/get
// response:
// { 
//     status: true,
//     data: [{ Code: "", Name: "" }]
// }
// 2. http://localhost:4000/v1/add
// request:
// {
//     Code: "",
//     Name: ""
// }
// response:
// { 
//     status: true,
//     message: "nếu có lỗi sẽ có giá trị"
// }
// 3. http://localhost:4000/v1/update
// request:
// {
//     Id: 0,
//     Code: "",
//     Name: ""
// }
// response:
// { 
//     status: true,
//     message: "nếu có lỗi sẽ có giá trị"
// }
// 4. http://localhost:4000/v1/delete
// request:
// {
//     Id: 0
// }
// response:
// { 
//     status: true,
//     message: "nếu có lỗi sẽ có giá trị"
// }
import moment from "moment";
String.prototype.interpolate = function (o) {
  return this.replace(/{([^{}]*)}/g, function (a, b) {
    var c = b.trim();
    var r = o[c];
    return typeof r === "string" || typeof r === "number" ? r : "";
  });
}

var listData = [];
var dataSearch = [];
let selectTotalData = 5;
// Load default Page
getData({
  url: "http://localhost:4000/v1/get",
  success: (data) => {
    listData = data.data;
    loadPage();
  }
})


function showData(listData, dataPage, curPage) {
  let codeSearch = $("#codeSearch").val()
  let nameSearch = $("#nameSearch").val()
  dataSearch = listData.filter(function (e) {
    return e.Code.indexOf(codeSearch) > -1 & e.Name.indexOf(nameSearch) > -1;
  });
  var template = $("#boxTemplate").html()
  var div = ""
  let startData = (curPage - 1) * dataPage;
  let endData = parseInt(startData) + parseInt(dataPage);
  if (dataSearch.length < endData) {
    endData = dataSearch.length;
  }
  let arrayDataShow = []
  for (let i = startData; i < endData; i++) {
    arrayDataShow.push(dataSearch[i]);
  }
  for (const obj of arrayDataShow) {
    div += template.interpolate({
      id: obj.Id,
      name: obj.Name,
      code: obj.Code,
      birthday: moment(obj.Birthday).format('MM:DD:YYYY'),
      image: obj.Image
    })
  }
  $("#showData").html(div)
}
$("#searchButton").on('click', function () {
  // set currentPage = 1;
  $('.active').html(1);
  loadPage();
})

$("#addData").on('click', function () {
  modal.show();
  let code = $("#code").val();
  let name = $("#name").val();
  let birthday = $("#birthday").val(); 
  let image = "";
  if($('input#fileUpdate')[0].files.length > 0 ){
    image = $('input#fileUpdate')[0].files[0].name;
  }
  addData({
    url: "http://localhost:4000/v1/add",
    data: {
      Code: code,
      Name: name,
      Birthday: birthday,
      Image: image
    },
    success: (data) => {
      console.log('add:')
      console.log(data)
      getData({
        url: "http://localhost:4000/v1/get",
        success: (data) => {
          console.log('get ajax:')
          console.log(data)
          listData = data.data;
          loadPage();
        }
      })
      $('#addModal').hide();
    }
  })
})

$("#updateData").on('click', function () {
  let code = $("#codeEdit").val()
  let name = $("#nameEdit").val()
  var checkbox = $(".checkboxId")
  var result = "";
  let image = "";
  for (var i = 0; i < checkbox.length; i++) {
    if (checkbox[i].checked === true) {
      result += checkbox[i].value;
    }
  }
  let birthday = $("#birthdayEdit").val();
  if($('input#fileUpdate')[0].files.length > 0 ){
    image = $('input#fileUpdate')[0].files[0].name;
  }
  updateData({
    url: "http://localhost:4000/v1/update",
    data: {
      Id: result,
      Code: code,
      Name: name,
      Birthday: birthday,
      Image: image
    },
    success: (data) => {
      console.log('update data:')
      console.log(data)
      getData({
        url: "http://localhost:4000/v1/get",
        success: (data) => {
          console.log('get ajax:')
          console.log(data)
          listData = data.data;
          loadPage();
        }
      })
      $('#editModal').hide();
    }
  })
})
$("#deleteData").on('click', function () {
  var checkbox = $(".checkboxId")
  var result = "";
  var arrayResult = [];
  for (var i = 0; i < checkbox.length; i++) {
    if (checkbox[i].checked === true) {
      result = checkbox[i].value;
      arrayResult.push(result);
    }
  }
  if(arrayResult == "") {
    alert("You need to select 1 record")
  } else {
    let text = "Are you sure to delete record ID : " + arrayResult;
    if (confirm(text) == true) {
      for (var i = 0; i < arrayResult.length; i++) {
        deleteData({
          url: "http://localhost:4000/v1/delete",
          data: {
            Id: arrayResult[i]
          },
          success: (data) => {
            console.log('delete ajax:')
            console.log(data)
          }
        })
        listData = listData.filter((list) => { return list.Id != arrayResult[i] })
      }
      alert("successfully deleted record ID : " +  arrayResult)
    }
    loadPage();
  }
})

$("#reload").on('click', function () {
  $('.active').html(1);
  $('#codeSearch').val("");
  $('#nameSearch').val("");
  loadPage();
})

async function getData(t) {
  $.ajax({
    url: t.url,
    headers: t.headers,
    type: "GET",
    success: function (data) {
      if (t.success) t.success(data);
    }
  });
}
function addData(t) {
  $.ajax({
    url: t.url,
    headers: t.headers,
    type: "POST",
    data: JSON.stringify(t.data),
    contentType: "application/json",
    success: function (data) {
      if (t.success) t.success(data);
    }
  });
}

function updateData(t) {
  $.ajax({
    url: t.url,
    headers: t.headers,
    type: "POST",
    data: JSON.stringify(t.data),
    contentType: "application/json",
    success: function (data) {
      if (t.success) t.success(data);
    }
  });
}

function deleteData(t) {
  $.ajax({
    url: t.url,
    headers: t.headers,
    type: "POST",
    data: JSON.stringify(t.data),
    contentType: "application/json",
    success: function (data) {
      if (t.success) t.success(data);
    }
  });
}
// Button Add
var modal = $('#addModal');
var addButton = $('#addButton');
var close = $('#close');

addButton.click(function () {
  $("[data-tag=imageAdd]").attr('src', 'assets/images/img/' )
  modal.show();
});

close.click(function () {
  modal.hide();
});

$(window).on('click', function (e) {
  if ($(e.target).is('#addModal')) {
    modal.hide();
  }
});
// Button Edit
var editModal = $('#editModal');
var editButton = $('#editButton');
var closeEdit = $('#closeEdit');

editButton.click(function () {
  var checkbox = $(".checkboxId")
  var result = 0
  for (var i = 0; i < checkbox.length; i++) {
    if (checkbox[i].checked === true) {
      result = checkbox[i].value;
      var detailData = listData.find((list) => { return list.Id == result })
      var format = moment(detailData.Birthday).format('YYYY/MM/DD');
      $("#codeEdit").val(detailData.Code);
      $("#nameEdit").val(detailData.Name);
      $("#birthdayEdit").val(format);
      $("[data-tag=image]").attr('src', 'assets/images/img/' + detailData.Image)
      editModal.show();
    } 
  }
  if(result == 0) {
    alert("You need to select 1 record")
  } 
});


closeEdit.click(function () {
  editModal.hide();
});

$(window).on('click', function (e) {
  if ($(e.target).is('#editModal')) {
    editModal.hide();
  }
});

//Paging
var pageNum = 0, pageOffset = 0;
function paging(baseElement, pages, pageShow) {

  function _initNav() {
    $('.nav-pages').html("")
    var curPage = $('.active').html();
    //create pages
    for (let i = 1; i < pages + 1; i++) {
      $((i == 1 ? '<li class="active">' : '<li>') + (i) + '</li>').appendTo('.nav-pages', baseElement).css('min-width', pages.toString().length + 'em');
    }

    //calculate initial values
    function ow(e) { return e.first().outerWidth() }
    var w = ow($('.nav-pages li', baseElement)), bw = ow($('.nav-btn', baseElement));
    baseElement.css('width', w * pageShow + (bw * 4) + 'px');
    $('.nav-pages', baseElement).css('margin-left', bw + 'px');

    //init events
    baseElement.on('click', '.nav-pages li, .nav-btn', function (e) {
      if ($(e.target).is('.nav-btn')) {
        var toPage = $(this).hasClass('prev') ? pageNum - 1 : pageNum + 1;
      } else {
        var toPage = $(this).index();
      }
      _navPage(toPage);
    });
  }

  function _navPage(toPage) {
    var sel = $('.nav-pages li', baseElement), w = sel.first().outerWidth(),
      diff = toPage - pageNum;

    if (toPage >= 0 && toPage <= pages - 1) {
      sel.removeClass('active').eq(toPage).addClass('active');
      pageNum = toPage;

    } else {
      return false;
    }
    if (toPage <= (pages - (pageShow + (diff > 0 ? 0 : 1))) && toPage >= 0) {
      pageOffset = pageOffset + -w * diff * 1.4 ;
      
    } else {
      pageOffset = (toPage > 0) ? -w * (pages - pageShow) * 1.4 : 0 ;
    }
    sel.parent().css('left', pageOffset + 'px');
    var curPage = $('.active').html();
    showData(listData, selectTotalData, curPage);
  }
  _initNav();
}
function showListPage(listData, selectTotalData) {
  let totalData = listData.length
  let totalPage = 0;
  if (totalData % selectTotalData == 0) {
    totalPage = totalData / selectTotalData
  } else {
    totalPage = Math.floor(totalData / selectTotalData) + 1;
  }
  paging($('.pagination'), totalPage, 5);
}



function ImageSetter(input,target) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function (e) {
          target.attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
  }
}
$(".imgInp").change(function(){
  var imgDiv=$(this).data('id');  
       imgDiv=$('#' + imgDiv);    
    ImageSetter(this,imgDiv);
  });


function loadPage() {
  var curPage = 1;
  showData(listData, selectTotalData, curPage);
  showListPage(dataSearch, selectTotalData); 
}
  